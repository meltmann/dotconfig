-- Keep Obsidian available in ALL THE nvim starts. As it is designed for know-
-- ledge management and knowledge comes whenever it wants I want quick-access
-- to my :ObsidianNew, :ObsidianOpen, :ObsidianToday and other commands, no
-- matter the file format or folder structure I'm currently in.
require'obsidian'.setup(
{
	workspaces = {
		{
			name = 'Marcos persönliches Wissensmanagement',
			path = '/home/' .. os.getenv("USER") .. '/PKM/',
		},
	},
	daily_notes = {
		folder = 'Periodic',
		alias_format = '%-d. %B %Y'
	},
}
)

-- lspconfig needs to setup Go Protocol Language Server for tinygo.nvim
require'lspconfig'.gopls.setup{}

-- tinygo.nvim itself has to be set up.
require'tinygo'.setup()
