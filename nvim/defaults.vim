"Stop getting confused with unnumbered lines by disabling line wrapping
"To keep track of the line lenght set a color marker at 80 as eyes didn't
"evolve as fast as monitors did. It even looks like we somehow auto-wrap if
"the line would be reached. That might get annoying later on...
"On the other hand the code lines itself seem to be safe against auto-wrapping
"functionality, which makes nvim an even more awesome code editor!
set nowrap colorcolumn=70,80,120

"Reduced tabstops for visibility.
set tabstop=2 softtabstop=2 shiftwidth=2

"Visualize all the invisible lines per default as it helps during coding alot
set list

"Enable relative line numbers for easier navigation and absolute active line
"number for easier orientation in the text files.
set relativenumber number

"Make things look awesome!
set termguicolors
"Catppuccin colorscheme is managed by Home Manager

set conceallevel=1
